using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class GameUI : MonoBehaviour
{
    public PlayerUIContainer[] playerContainers;
    public TextMeshProUGUI winText;

    [Header("Component")]

    public PhotonView photonView;

    private float updateTimer;
    public static GameUI instance;
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        InitializePlayerUI();
    }
    void Update()
    {
        UpdatePlayerUI();
    }
    void InitializePlayerUI()
    {
        for(int x=0;x< playerContainers.Length;++x)
        {
            PlayerUIContainer container = playerContainers[x];

            if(x< PhotonNetwork.PlayerList.Length)
            {
                container.Object.SetActive(true);
                container.nameText.text = PhotonNetwork.PlayerList[x].NickName;
                container.MushroomTimeSlider.maxValue = GameManager.instance.timeToWin;
            }
            else{
                container.Object.SetActive(false);
            }
        }
    }
      void UpdatePlayerUI()
    {
    for (int x=0; x< GameManager.instance.players.Length; ++x)
    {
        if(GameManager.instance.players[x] != null)
        playerContainers[x].MushroomTimeSlider.value = GameManager.instance.players[x].curMushTime;
    }
    }
    public void SetWinText(string winnerName)
    {
        winText.gameObject.SetActive(true);
        winText.text = winnerName + " wins";
    }
}

  

[System.Serializable]
public class PlayerUIContainer
{
    public GameObject Object;
    public TextMeshProUGUI nameText;
    public Slider MushroomTimeSlider;
}
