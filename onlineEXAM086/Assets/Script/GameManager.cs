using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;

public class GameManager : MonoBehaviourPunCallbacks
{
    [Header("Stats")]
    [HideInInspector]
    public bool gameEnd = false;
    public float timeToWin;
    public float invincibleTime;
    private float MushPick;

    [Header("Player")]
    public string playerPrefeb;
    public Transform[] spawnPoint;
    [HideInInspector]
    public Movement[] players;
    [HideInInspector]
    public int playerWithMush;
    private int playerInGame;
    [Header("Components")]
    public PhotonView photonView;


    public static GameManager instance;

    void Awake()
    {
        instance =this;
    }

    void Start()
    {
        players = new Movement[PhotonNetwork.PlayerList.Length];
        photonView.RPC("ImInGame", RpcTarget.AllBuffered);
    }

    [PunRPC]
    void ImInGame()
    {
        playerInGame++;

        if (playerInGame == PhotonNetwork.PlayerList.Length)
        {
            spawnPlayer();
        }
    }

    void spawnPlayer()
    {
        GameObject  playerObj = PhotonNetwork.Instantiate(playerPrefeb, spawnPoint[Random.Range(0,spawnPoint.Length)].position,Quaternion.identity);
        Movement playerScript = playerObj.GetComponent<Movement>();

        playerScript.photonView.RPC("Initialize", RpcTarget.All, PhotonNetwork.LocalPlayer);
  }

    public Movement GetPlayer (int playerId)
    {
        return players.First(x => x.id == playerId);
    }

    public Movement GetPlayer(GameObject playerObj)
    {
        return players.First(x => x.gameObject == playerObj);
    }

    [PunRPC]
    public void GiveMush (int playerId, bool initialGive = false)
    {
        if(!initialGive)
            GetPlayer(playerWithMush).SetHat(false);

            playerWithMush = playerId;
            GetPlayer(playerId).SetHat(true);
            MushPick =Time.time;
    }

    public bool CanGetMushroom()
    {
        if(Time.time > MushPick + invincibleTime)
            return true;
        else
        return false;
    }

    [PunRPC]
    void WinGame(int playerId)
    {
        gameEnd = true;
        Movement Player = GetPlayer(playerId);
        GameUI.instance.SetWinText(Player.photonPlayer.NickName);
        
        Invoke("goBackToMenu", 5.0f);

    }

    void goBackToMenu()
    {
        PhotonNetwork.LeaveRoom();
        NewNetworkManager.instance.ChangeScene("Menu");
    }
}
