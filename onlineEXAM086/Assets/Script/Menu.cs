using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Menu : MonoBehaviourPunCallbacks
{
   [Header("screen")]
   public GameObject mainScreen;
   public GameObject lobbyScreen;


   [Header("Main Screen")]
   public Button createRoomButton;
   public Button joinRoomButton;

   [Header("Lobby Screen")]
   public Text PlayerListText;
   public Button startGameButton;


   void Start()
   {
       createRoomButton.interactable = false;
       joinRoomButton.interactable = false;
   }

   public override void OnConnectedToMaster()
   {
       createRoomButton.interactable = true;
       joinRoomButton.interactable = true;
   }

   void SetScreen (GameObject screen)
   {
       mainScreen.SetActive(false);
       lobbyScreen.SetActive(false);

       screen.SetActive(true);
   }

   public void OnCreateRoomButton(InputField roomNameInput)
   {
       NewNetworkManager.instance.CreateRoom(roomNameInput.text);
   }

   public void OnJoinRoomButton(InputField roomNameInput)
   {
       NewNetworkManager.instance.JoinRoom(roomNameInput.text);
   }

   public void OnPlayerNameUpdate(InputField PlayernameInput)
   {
       PhotonNetwork.NickName = PlayernameInput.text;
   }

    public override void OnJoinedRoom()
    {
        SetScreen(lobbyScreen);

        photonView.RPC("UpdateLobbyUI",RpcTarget.All);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        UpdateLobbyUI();
    }

    [PunRPC]
    public void UpdateLobbyUI()
    {
        PlayerListText.text = "";

        foreach(Player player in PhotonNetwork.PlayerList)
        {
            PlayerListText.text += player.NickName +"\n";
        }

        if(PhotonNetwork.IsMasterClient)
        {
            startGameButton.interactable = true;
        }
        else
        {
            startGameButton.interactable = false;
        }
            
        
    }

    public void OnLeaveLobbyButton()
    {
        PhotonNetwork.LeaveRoom();
        SetScreen(mainScreen);
    }

    public void OnStartGameButton()
    {
        NewNetworkManager.instance.photonView.RPC("ChangeScene",RpcTarget.All,"Gameplay");
    }
}
