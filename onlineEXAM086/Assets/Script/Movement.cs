using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class Movement : MonoBehaviourPunCallbacks, IPunObservable
{
    [HideInInspector]
    public int id;

    [Header("Info")]
    public float speed;
    public float jump;
    public GameObject MushRoomOP;
   // public CharacterController controller;
   
   // public float gravity = 9.8f;
 
    [HideInInspector]
    public float curMushTime;

    [Header("Componants")]
    public Player photonPlayer;
    public Rigidbody rrb;

    [PunRPC]
    public void Initialize (Player player)
    {
      photonPlayer = player;
      id = player.ActorNumber;

      GameManager.instance.players[id - 1] = this;


      if(id ==1)
      {
        GameManager.instance.GiveMush(id, true);
      }

      if(!photonView.IsMine)
      {
        rrb.isKinematic = true;
      }
    }



   
    // Update is called once per frame
    void Update()
    {

      if(PhotonNetwork.IsMasterClient)
      {
        if (curMushTime >= GameManager.instance.timeToWin && !GameManager.instance.gameEnd)
        {
            GameManager.instance.gameEnd = true;
            GameManager.instance.photonView.RPC("WinGame", RpcTarget.All, id);
        }
      }

      if(photonView.IsMine)
      {
        Move();
        if(Input.GetKeyDown(KeyCode.Space))
        {
          GoJump();


          if(MushRoomOP.activeInHierarchy)
          curMushTime += Time.deltaTime;
        }
      }

    }
    
    void Move()
    {
        float x = Input.GetAxis("Horizontal")* speed;
        float z = Input.GetAxis("Vertical") * speed;

       // Vector3 direction = new Vector3 (horizontalInput,0,verticalInput);
        rrb.velocity = new Vector3(x,rrb.velocity.y,z);
        //direction.y -= gravity;
      //controller.Move(direction * speed *Time.deltaTime);
     
    }

    void GoJump()
    {
          Ray ray = new Ray (transform.position,Vector3.down);
          if(Physics.Raycast(ray,0.7f))
          {
            rrb.AddForce(Vector3.up *jump,  ForceMode.Impulse);
          }
    }

    public void SetHat(bool hasHat)
    {
      MushRoomOP.SetActive(hasHat);
    }

    void OnCollisionEnter (Collision collision)
    {
      if(!photonView.IsMine)
        return;

        if(collision.gameObject.CompareTag("Player"))
        {
          if(GameManager.instance.GetPlayer(collision.gameObject).id == GameManager.instance.playerWithMush)
          {
            if(GameManager.instance.CanGetMushroom())
            {
              GameManager.instance.photonView.RPC("GiveMush", RpcTarget.All, id,false);
            }
          }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
          stream.SendNext(curMushTime);
        }
        else if(stream.IsReading)
        {
          curMushTime = (float)stream.ReceiveNext();
        }
    }
    
}
